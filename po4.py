''' 
Simple Pomodoro timer in python
'''

try:
    # Python2
    import Tkinter as tk
    import Tkinter.tkMessageBox as tkbox
except ImportError:
    # Python3
    import tkinter as tk
    import tkinter.messagebox as tkbox
    import time

import csv

def time_overloaded():
    current_color.set('red')
    time_label.configure(fg=current_color.get())

def time_restarted():
    current_color.set('green')
    time_label.configure(fg=current_color.get())

def color_toggle():
    if current_color.get() == 'green' :
        current_color.set('red')
    else:
        current_color.set('green')
    time_label.configure(fg=current_color.get())


def count_down(minutes):
    # start with 2 minutes --> 120 seconds
    for t in range(minutes*60, -1, -1):
        # format as 2 digit integers, fills with zero to the left
        # divmod() gives minutes, seconds
        sf = "{:02d}:{:02d}".format(*divmod(t, 60))
        #print(sf) # test
        time_str.set(sf)
        root.update()
        # delay one second
        time.sleep(1)
    time_str.set(" ")

def count(minutes):
    time_overloaded()
    t = minutes*60;
    while 1:
        # format as 2 digit integers, fills with zero to the left
        # divmod() gives minutes, seconds
        sf = "-{:02d}:{:02d}".format(*divmod(t, 60))
        #print(sf) # test
        time_str.set(sf)
        root.update()
        # delay one second
        time.sleep(1)
        t = t + 1

def pomodoro():
    time_restarted()
    count_down(25)
    count(25)
    tkbox.showinfo(title="Greetings", message="Done!")
    log_time(25, "Pomodoro");

def short_pause():
    time_restarted()
    count_down(5)
    count(5)
    tkbox.showinfo(title="Greetings", message="Finished!")
    log_time(5, "Short Pause");

def long_pause():
    time_restarted()
    count_down(15)
    count(15)
    tkbox.showinfo(title="Greetings", message="Finished!")
    log_time(15, "Long Pause");

def log_time(time_spent, pom_type):
    with open('pomodoro.log', 'a') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=';',
                             quotechar='\"', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow([time.strftime("%d/%m/%Y"),time.strftime("%H:%M:%S"), time_spent, pom_type])

# create root/main window
root = tk.Tk()
root.title("Pomodoro")
time_str = tk.StringVar()

# Label color
current_color = tk.StringVar()
current_color.set('green')
# create the time display label, give it a large font
# label auto-adjusts to the font
label_font = ('helvetica', 40)
time_label = tk.Label(root, textvariable=time_str, font=label_font, bg='black',
         fg=current_color.get(), relief='raised', bd=3)
time_label.pack(fill='x', padx=5, pady=5)

# create start and stop buttons
# pack() positions the buttons below the label
tk.Button(root, text='Start Pomodoro', command=pomodoro).pack({"side": "left"})
tk.Button(root, text='Short Pause', command=short_pause).pack({"side": "left"})
tk.Button(root, text='Long Pause', command=long_pause).pack({"side": "left"})

# start the GUI event loop
root.mainloop()
